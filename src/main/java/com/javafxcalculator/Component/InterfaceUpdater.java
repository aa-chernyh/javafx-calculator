package com.javafxcalculator.Component;

import com.javafxcalculator.DTO.ResultDTO;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.util.LinkedList;

public class InterfaceUpdater {
    private final String RESULT_DELIMITER = "  =  ";

    private static InterfaceUpdater instance = null;

    private Label inputLabel = null;
    private Label errorLabel = null;
    private VBox resultsScrollPaneVbox = null;

    private InterfaceUpdater() {}

    public static InterfaceUpdater getInstance() {
        if (InterfaceUpdater.instance == null) {
            InterfaceUpdater.instance = new InterfaceUpdater();
        }

        return InterfaceUpdater.instance;
    }

    public void setInputLabel(Label label) {
        this.inputLabel = label;
    }

    public void setErrorLabel(Label label) {
        this.errorLabel = label;
    }

    public void setResultsScrollPaneVbox(VBox vbox) {
        this.resultsScrollPaneVbox = vbox;
    }

    public void setInputText(String text) {
        if (this.inputLabel != null) {
            this.inputLabel.setText(text);
        }
    }

    public void setErrorText(String text) {
        if (this.errorLabel != null) {
            this.errorLabel.setText(text);
        }
    }

    public void updateResultsVBox(LinkedList<ResultDTO> resultList) {
        if (this.resultsScrollPaneVbox == null) {
            return;
        }

        this.resultsScrollPaneVbox.getChildren().clear();

        int itemsCount = resultList.size();
        for (int i = 0; i < itemsCount; i++) {
            Label label = new Label();

            ResultDTO resultDTO = resultList.get(i);
            label.setText(resultDTO.expression + this.RESULT_DELIMITER + resultDTO.result);
            if ((i + 1) == itemsCount) {
                label.getStyleClass().add("last");
            }

            this.resultsScrollPaneVbox.getChildren().add(label);
        }
    }
}
