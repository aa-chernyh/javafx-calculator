package com.javafxcalculator.Component;

import com.ezylang.evalex.EvaluationException;
import com.ezylang.evalex.parser.ParseException;
import com.javafxcalculator.DTO.ResultDTO;
import com.javafxcalculator.Enum.Symbol;
import com.javafxcalculator.Helper.StringHelper;
import com.javafxcalculator.Model.State;
import com.javafxcalculator.Service.AdaptStringForCalculateService;
import com.ezylang.evalex.Expression;
import com.javafxcalculator.Service.OpenAboutWindowService;

public class ButtonActions {
    private State state = State.getInstance();

    public void add(Symbol symbol) {
        this.state.clearErrorText();

        String newText = this.state.getInputText() + symbol.getValue();
        this.state.setInputText(newText);
    }

    public void cutLast() {
        this.state.clearErrorText();

        if (this.state.getInputText().length() == 0) {
            return;
        }

        if (this.state.getInputText().equals(this.state.getLastCalculatedResult())) {
            this.state.clearLastCalculatedResult();
            this.state.clearInputText();
        }

        String newString = StringHelper.removeLastChar(this.state.getInputText());
        this.state.setInputText(newString);
    }

    public void calculate() {
        this.state.clearErrorText();

        try {
            AdaptStringForCalculateService service = new AdaptStringForCalculateService(this.state.getInputText());
            String stringForCalculate = service.service();

            Expression expression = new Expression(stringForCalculate);
            this.state.setLastCalculatedResult(expression.evaluate().getStringValue());
        } catch (EvaluationException | ParseException | RuntimeException e) {
            this.state.setErrorText(e.getMessage());
            return;
        }

        ResultDTO resultDTO = new ResultDTO(this.state.getInputText(), this.state.getLastCalculatedResult());
        this.state.addResult(resultDTO);

        this.state.setInputText(this.state.getLastCalculatedResult());
    }

    public void openAboutWindow() {
        OpenAboutWindowService service = new OpenAboutWindowService();
        service.service();
    }
}
