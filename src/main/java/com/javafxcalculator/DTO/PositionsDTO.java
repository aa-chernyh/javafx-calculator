package com.javafxcalculator.DTO;

public class PositionsDTO {
    public int start;
    public int end;

    public PositionsDTO(int start, int end) {
        this.start = start;
        this.end = end;
    }
}
