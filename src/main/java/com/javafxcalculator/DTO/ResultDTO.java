package com.javafxcalculator.DTO;

public class ResultDTO {
    public String expression;
    public String result;

    public ResultDTO(String expression, String result) {
        this.expression = expression;
        this.result = result;
    }
}
