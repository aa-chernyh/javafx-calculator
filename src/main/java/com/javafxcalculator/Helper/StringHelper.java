package com.javafxcalculator.Helper;

import java.util.Optional;

public class StringHelper {
    public static String removeLastChar(String s) {
        return Optional.ofNullable(s)
            .map(str -> str.replaceAll(".$", ""))
            .orElse(s);
    }
}
