package com.javafxcalculator.Enum;

public enum Symbol {
    ZERO('0'),
    ONE('1'),
    TWO('2'),
    THREE('3'),
    FOUR('4'),
    FIVE('5'),
    SIX('6'),
    SEVEN('7'),
    EIGHT('8'),
    NINE('9'),
    ADDITION('+'),
    DOT('.'),
    DIVISION('÷'),
    LEFT_PARENTHESIS('('),
    FACTORIAL('!'),
    MULTIPLICATION('×'),
    PI('π'),
    RIGHT_PARENTHESIS(')'),
    SQUARE_ROOT('√'),
    SUBTRACTION('-'),
    X_SQUARED('²'),
    ;

    private final char value;

    private Symbol(char value) {
        this.value = value;
    }

    public char getValue() {
        return this.value;
    }
}
