package com.javafxcalculator.Controller;

import com.javafxcalculator.Component.ButtonActions;
import com.javafxcalculator.Component.InterfaceUpdater;
import com.javafxcalculator.Enum.Symbol;
import com.javafxcalculator.Service.DisableButtonFocusService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class CalculatorController {
    @FXML
    private Label errorLabel;

    @FXML
    private Label insertLabel;

    @FXML
    private Button numpad0;

    @FXML
    private Button numpad1;

    @FXML
    private Button numpad2;

    @FXML
    private Button numpad3;

    @FXML
    private Button numpad4;

    @FXML
    private Button numpad5;

    @FXML
    private Button numpad6;

    @FXML
    private Button numpad7;

    @FXML
    private Button numpad8;

    @FXML
    private Button numpad9;

    @FXML
    private Button numpadAddition;

    @FXML
    private Button numpadBackspace;

    @FXML
    private Button numpadDot;

    @FXML
    private Button numpadDivision;

    @FXML
    private Button numpadEquals;

    @FXML
    private Button numpadLeftParenthesis;

    @FXML
    private Button numpadFactorial;

    @FXML
    private Button numpadMultiplication;

    @FXML
    private Button numpadAbout;

    @FXML
    private Button numpadPi;

    @FXML
    private Button numpadRightParenthesis;

    @FXML
    private Button numpadSquareRoot;

    @FXML
    private Button numpadSubtraction;

    @FXML
    private Button numpadXSquared;

    @FXML
    private VBox resultsScrollPaneVbox;

    private ButtonActions buttonActions = new ButtonActions();

    @FXML
    public void initialize() {
        InterfaceUpdater interfaceUpdater = InterfaceUpdater.getInstance();
        interfaceUpdater.setInputLabel(this.insertLabel);
        interfaceUpdater.setErrorLabel(this.errorLabel);
        interfaceUpdater.setResultsScrollPaneVbox(this.resultsScrollPaneVbox);

        DisableButtonFocusService service = new DisableButtonFocusService(this);
        service.service();
    }

    @FXML
    void on0Pressed(ActionEvent event) {
        this.buttonActions.add(Symbol.ZERO);
    }

    @FXML
    void on1Pressed(ActionEvent event) {
        this.buttonActions.add(Symbol.ONE);
    }

    @FXML
    void on2Pressed(ActionEvent event) {
        this.buttonActions.add(Symbol.TWO);
    }

    @FXML
    void on3Pressed(ActionEvent event) {
        this.buttonActions.add(Symbol.THREE);
    }

    @FXML
    void on4Pressed(ActionEvent event) {
        this.buttonActions.add(Symbol.FOUR);
    }

    @FXML
    void on5Pressed(ActionEvent event) {
        this.buttonActions.add(Symbol.FIVE);
    }

    @FXML
    void on6Pressed(ActionEvent event) {
        this.buttonActions.add(Symbol.SIX);
    }

    @FXML
    void on7Pressed(ActionEvent event) {
        this.buttonActions.add(Symbol.SEVEN);
    }

    @FXML
    void on8Pressed(ActionEvent event) {
        this.buttonActions.add(Symbol.EIGHT);
    }

    @FXML
    void on9Pressed(ActionEvent event) {
        this.buttonActions.add(Symbol.NINE);
    }

    @FXML
    void onAdditionPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.ADDITION);
    }

    @FXML
    void onBackspacePressed(ActionEvent event) {
        this.buttonActions.cutLast();
    }

    @FXML
    void onDotPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.DOT);
    }

    @FXML
    void onDivisionPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.DIVISION);
    }

    @FXML
    void onEqualsPressed(ActionEvent event) {
        this.buttonActions.calculate();
    }

    @FXML
    void onLeftParenthesisPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.LEFT_PARENTHESIS);
    }

    @FXML
    void onFactorialPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.FACTORIAL);
    }

    @FXML
    void onMultiplicationPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.MULTIPLICATION);
    }

    @FXML
    void onAboutPressed(ActionEvent event) {
        this.buttonActions.openAboutWindow();
    }

    @FXML
    void onPiPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.PI);
    }

    @FXML
    void onRightParenthesisPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.RIGHT_PARENTHESIS);
    }

    @FXML
    void onSquareRootPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.SQUARE_ROOT);
    }

    @FXML
    void onSubstractionPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.SUBTRACTION);
    }

    @FXML
    void onXSquaredPressed(ActionEvent event) {
        this.buttonActions.add(Symbol.X_SQUARED);
    }
}