package com.javafxcalculator.Service;

import com.javafxcalculator.Component.ButtonActions;
import com.javafxcalculator.Model.Data.ApplicationData;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class OpenAboutWindowService {
    private final String FXML_PATH = "/com/javafxcalculator/about.fxml";
    private final String WINDOW_TITLE = "About Calculator";
    private final int WINDOW_HEIGHT = 200;

    public void service() {
        Parent root;
        try {
            root = FXMLLoader.load(ButtonActions.class.getResource(this.FXML_PATH));
            Stage stage = new Stage();
            stage.setTitle(this.WINDOW_TITLE);
            stage.setScene(new Scene(root));
            stage.setMinWidth(ApplicationData.WIDTH);
            stage.setMinHeight(this.WINDOW_HEIGHT);
            stage.setWidth(ApplicationData.WIDTH);
            stage.setHeight(this.WINDOW_HEIGHT);
            stage.show();
        }
        catch (IOException e) {
            throw new RuntimeException("Can't open \"About\" window: " + e.getMessage());
        }
    }
}
