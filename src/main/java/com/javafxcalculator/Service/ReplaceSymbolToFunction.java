package com.javafxcalculator.Service;

import com.javafxcalculator.DTO.PositionsDTO;
import com.javafxcalculator.Enum.Symbol;

import java.util.*;

public class ReplaceSymbolToFunction {
    private final Set<Character> NUMBER_CHARS = Set.of(
        Symbol.ZERO.getValue(),
        Symbol.ONE.getValue(),
        Symbol.TWO.getValue(),
        Symbol.THREE.getValue(),
        Symbol.FOUR.getValue(),
        Symbol.FIVE.getValue(),
        Symbol.SIX.getValue(),
        Symbol.SEVEN.getValue(),
        Symbol.EIGHT.getValue(),
        Symbol.NINE.getValue(),
        Symbol.DOT.getValue()
    );

    private char[] string;
    private Symbol symbol;
    private String funcName;
    private boolean isSymbolOnLeft;
    
    public ReplaceSymbolToFunction(String string, Symbol symbol, String funcName, boolean isSymbolOnLeft) {
        this.string = string.toCharArray();
        this.symbol = symbol;
        this.funcName = funcName;
        this.isSymbolOnLeft = isSymbolOnLeft;
    }
    
    public String service() {
        List<Integer> symbolPositionList = this.getSymbolPositionList();
        List<PositionsDTO> expressionsPosition = this.isSymbolOnLeft
            ? this.getExpressionsPositionForSymbolOnLeft(symbolPositionList)
            : this.getExpressionsPositionForSymbolOnRight(symbolPositionList);

        return this.replaceCharsToFunctions(symbolPositionList, expressionsPosition);
    }

    private List<Integer> getSymbolPositionList() {
        List<Integer> symbolPositionList = new ArrayList<>();
        for (int position = 0; position < this.string.length; position++) {
            if (this.string[position] == this.symbol.getValue()) {
                symbolPositionList.add(position);
            }
        }

        return symbolPositionList;
    }

    private List<PositionsDTO> getExpressionsPositionForSymbolOnLeft(List<Integer> symbolPositionList) {
        List<PositionsDTO> expressionsPosition = new ArrayList<>();
        for (int symbolPosition : symbolPositionList) {
            int position = symbolPosition + 1;
            if (position > this.string.length - 1) {
                String message = String.format("Symbol %s cannot appear at the ending of an expression", this.symbol.getValue());
                throw new RuntimeException(message);
            }

            int parenthesisCount = 0;
            do {
                if (this.string[position] == '(') {
                    parenthesisCount++;
                }

                if (parenthesisCount > 0 && this.string[position] == ')') {
                    parenthesisCount--;
                }

                position++;
            } while (position < this.string.length && (parenthesisCount != 0 || this.NUMBER_CHARS.contains(this.string[position])));

            expressionsPosition.add(new PositionsDTO(symbolPosition + 1, position - 1));
        }

        return expressionsPosition;
    }

    private List<PositionsDTO> getExpressionsPositionForSymbolOnRight(List<Integer> symbolPositionList) {
        List<PositionsDTO> expressionsPosition = new ArrayList<>();
        for (int symbolPosition : symbolPositionList) {
            int position = symbolPosition - 1;
            if (position < 0) {
                String message = String.format("Symbol %s cannot appear at the beginning of an expression", this.symbol.getValue());
                throw new RuntimeException(message);
            }

            int parenthesisCount = 0;
            do {
                if (this.string[position] == ')') {
                    parenthesisCount++;
                }

                if (parenthesisCount > 0 && this.string[position] == '(') {
                    parenthesisCount--;
                }

                position--;
            } while (position >= 0 && (parenthesisCount != 0 || this.NUMBER_CHARS.contains(this.string[position])));

            expressionsPosition.add(new PositionsDTO(position + 1, symbolPosition - 1));
        }

        return expressionsPosition;
    }

    private String replaceCharsToFunctions(List<Integer> symbolPositionList, List<PositionsDTO> expressionsPosition) {
        StringBuilder builder = new StringBuilder();
        for (int position = 0; position < this.string.length; position++) {
            if (symbolPositionList.contains(position)) {
                continue;
            }

            for (PositionsDTO dto : expressionsPosition) {
                if (position == dto.start) {
                    builder.append(this.funcName).append('(');
                }
            }

            builder.append(this.string[position]);

            for (PositionsDTO dto : expressionsPosition) {
                if (position == dto.end) {
                    builder.append(')');
                }
            }
        }

        return builder.toString();
    }
}
