package com.javafxcalculator.Service;

import com.javafxcalculator.Controller.CalculatorController;
import javafx.scene.control.Button;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DisableButtonFocusService {
    CalculatorController controller;

    public DisableButtonFocusService(CalculatorController controller) {
        this.controller = controller;
    }

    public void service() {
        for (Field field : this.getReflectionFields(this.controller)) {
            if (field.getType() == Button.class) {
                try {
                    field.setAccessible(true);
                    Button button = (Button) field.get(this.controller);
                    button.setFocusTraversable(false);
                } catch (IllegalAccessException e) {
                    // do nothing
                }
            }
        }
    }

    private <T> List<Field> getReflectionFields(T t) {
        List<Field> fields = new ArrayList<>();
        Class clazz = t.getClass();
        while (clazz != Object.class) {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }
        return fields;
    }
}
