package com.javafxcalculator.Service;

import com.javafxcalculator.Component.ButtonActions;
import com.javafxcalculator.Enum.Symbol;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class HandleKeyboardService {
    private Stage stage;
    private ButtonActions buttonActions = new ButtonActions();
    private static boolean isShiftPressed = false;

    public HandleKeyboardService(Stage stage) {
        this.stage = stage;
    }

    public void service() {
        this.handleKeyPressed();
        this.handleKeyReleased();
    }

    private void handleKeyPressed() {
        this.stage.addEventHandler(KeyEvent.KEY_PRESSED, keyEvent -> {
            switch (keyEvent.getCode()) {
                case SHIFT:
                    HandleKeyboardService.isShiftPressed = true;
                    return;
                case EQUALS:
                    if (HandleKeyboardService.isShiftPressed) {
                        this.buttonActions.add(Symbol.ADDITION);
                    } else {
                        this.buttonActions.calculate();
                    }
                    return;
                case ENTER:
                    this.buttonActions.calculate();
                    return;
                case BACK_SPACE:
                case DELETE:
                    this.buttonActions.cutLast();
                    return;
                case I:
                    this.buttonActions.openAboutWindow();
                default:
                    Symbol symbol = this.getSymbolByCode(keyEvent.getCode());
                    if (symbol == null) {
                        return;
                    }

                    this.buttonActions.add(symbol);
            }
        });
    }

    private Symbol getSymbolByCode(KeyCode keyCode) {
        switch (keyCode) {
            case DIGIT0:
                return HandleKeyboardService.isShiftPressed ? Symbol.RIGHT_PARENTHESIS : Symbol.ZERO;
            case NUMPAD0:
                return Symbol.ZERO;
            case DIGIT1:
                return HandleKeyboardService.isShiftPressed ? Symbol.FACTORIAL : Symbol.ONE;
            case NUMPAD1:
                return Symbol.ONE;
            case DIGIT2:
            case NUMPAD2:
                return Symbol.TWO;
            case DIGIT3:
            case NUMPAD3:
                return Symbol.THREE;
            case DIGIT4:
            case NUMPAD4:
                return Symbol.FOUR;
            case DIGIT5:
            case NUMPAD5:
                return Symbol.FIVE;
            case DIGIT6:
                return HandleKeyboardService.isShiftPressed ? Symbol.X_SQUARED : Symbol.SIX;
            case NUMPAD6:
                return Symbol.SIX;
            case DIGIT7:
            case NUMPAD7:
                return Symbol.SEVEN;
            case DIGIT8:
                return HandleKeyboardService.isShiftPressed ? Symbol.MULTIPLICATION : Symbol.EIGHT;
            case NUMPAD8:
                return Symbol.EIGHT;
            case DIGIT9:
                return HandleKeyboardService.isShiftPressed ? Symbol.LEFT_PARENTHESIS : Symbol.NINE;
            case NUMPAD9:
                return Symbol.NINE;
            case ADD:
                return Symbol.ADDITION;
            case COMMA:
            case DECIMAL:
            case PERIOD:
                return Symbol.DOT;
            case DIVIDE:
            case SLASH:
                return Symbol.DIVISION;
            case LEFT_PARENTHESIS:
                return Symbol.LEFT_PARENTHESIS;
            case EXCLAMATION_MARK:
                return Symbol.FACTORIAL;
            case MULTIPLY:
                return Symbol.MULTIPLICATION;
            case P:
                return Symbol.PI;
            case RIGHT_PARENTHESIS:
                return Symbol.RIGHT_PARENTHESIS;
            case SUBTRACT:
            case MINUS:
                return Symbol.SUBTRACTION;
        }

        return null;
    }

    private void handleKeyReleased() {
        this.stage.addEventHandler(KeyEvent.KEY_RELEASED, keyEvent -> {
            switch (keyEvent.getCode()) {
                case SHIFT:
                    HandleKeyboardService.isShiftPressed = false;
                    return;
            }
        });
    }
}
