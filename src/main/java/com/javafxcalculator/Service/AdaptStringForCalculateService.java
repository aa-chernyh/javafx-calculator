package com.javafxcalculator.Service;

import com.javafxcalculator.Enum.Symbol;

public class AdaptStringForCalculateService {
    private String string;

    public AdaptStringForCalculateService(String string) {
        this.string = string;
    }

    public String service() {
        this.replaceFactorial();
        this.replaceXSquared();
        this.replaceDivision();
        this.replaceMultiplication();
        this.replacePi();
        this.replaceSquareRoot();

        return this.string;
    }

    private void replaceDivision() {
        this.string = this.string.replace(Symbol.DIVISION.getValue(), '/');
    }

    private void replaceFactorial() {
        ReplaceSymbolToFunction service = new ReplaceSymbolToFunction(this.string, Symbol.FACTORIAL, "FACT", false);
        this.string = service.service();
    }

    private void replaceMultiplication() {
        this.string = this.string.replace(Symbol.MULTIPLICATION.getValue(), '*');
    }

    private void replacePi() {
        this.string = this.string.replace(String.valueOf(Symbol.PI.getValue()), "PI");
    }

    private void replaceSquareRoot() {
        ReplaceSymbolToFunction service = new ReplaceSymbolToFunction(this.string, Symbol.SQUARE_ROOT, "SQRT", true);
        this.string = service.service();
    }

    private void replaceXSquared() {
        this.string = this.string.replace(String.valueOf(Symbol.X_SQUARED.getValue()), "^2");
    }
}
