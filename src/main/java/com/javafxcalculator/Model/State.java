package com.javafxcalculator.Model;

import com.javafxcalculator.Component.InterfaceUpdater;
import com.javafxcalculator.DTO.ResultDTO;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.util.LinkedList;

public class State {
    private static State instance = null;

    private final int RESULTS_MAX_COUNT = 64;

    private String inputText = "";
    private String errorText = "";
    private LinkedList<ResultDTO> resultList = new LinkedList<>();
    private String lastCalculatedResult = "";

    private InterfaceUpdater interfaceUpdater = InterfaceUpdater.getInstance();

    private State() {}

    public static State getInstance() {
        if (State.instance == null) {
            State.instance = new State();
        }

        return State.instance;
    }

    public String getInputText() {
        return this.inputText;
    }

    public void setInputText(String text) {
        this.inputText = text;
        this.interfaceUpdater.setInputText(text);
    }

    public void clearInputText() {
        this.setInputText("");
    }

    public String getErrorText() {
        return this.errorText;
    }

    public void setErrorText(String text) {
        this.errorText = text;
        this.interfaceUpdater.setErrorText(text);
    }

    public void clearErrorText() {
        this.setErrorText("");
    }

    public void addResult(ResultDTO result) {
        if (this.resultList.size() >= this.RESULTS_MAX_COUNT) {
            this.resultList.pollLast();
        }

        this.resultList.addFirst(result);
        this.interfaceUpdater.updateResultsVBox(this.resultList);
    }

    public String getLastCalculatedResult() {
        return this.lastCalculatedResult;
    }

    public void setLastCalculatedResult(String result) {
        this.lastCalculatedResult = result;
    }

    public void clearLastCalculatedResult() {
        this.setLastCalculatedResult("");
    }
}
