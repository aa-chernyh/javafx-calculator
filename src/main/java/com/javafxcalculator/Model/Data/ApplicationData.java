package com.javafxcalculator.Model.Data;

public class ApplicationData {
    public final static int WIDTH = 370;
    public final static int HEIGHT = 537;
    public final static String NAME = "Calculator";
    public final static String ICON_PATH = "/com/javafxcalculator/img/icon.png";
}
