package com.javafxcalculator;

import com.javafxcalculator.Model.Data.ApplicationData;
import com.javafxcalculator.Service.HandleKeyboardService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import java.io.IOException;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("calculator.fxml"));
        Scene scene = new Scene(fxmlLoader.load());

        stage.getIcons().add(new Image(getClass().getResourceAsStream(ApplicationData.ICON_PATH)));
        stage.setTitle(ApplicationData.NAME);
        stage.setScene(scene);
        stage.setMinWidth(ApplicationData.WIDTH);
        stage.setMinHeight(ApplicationData.HEIGHT);
        stage.setWidth(ApplicationData.WIDTH);
        stage.setHeight(ApplicationData.HEIGHT);
        stage.show();

        HandleKeyboardService service = new HandleKeyboardService(stage);
        service.service();
    }

    public static void main(String[] args) {
        launch();
    }
}