module com.javafxcalculator {
    requires javafx.controls;
    requires javafx.fxml;
    requires EvalEx;

    opens com.javafxcalculator to javafx.fxml;
    exports com.javafxcalculator;
    exports com.javafxcalculator.Controller;
    opens com.javafxcalculator.Controller to javafx.fxml;
}
