default: start

version = 0.9.0
jar_file = javafx-calculator-$(version)-jar-with-dependencies.jar
jar_path = target/$(jar_file)

start:
	mvn javafx:run
clean:
	mvn clean
install:
	mvn install
reinstall:
	rm -rf ~/.m2/repository && mvn clean install -U
run-jar:
	java -jar $(jar_path)
build-jar:
	docker run -it --rm -v `pwd`:/app -w /app maven:3.8.5-openjdk-17-slim mvn clean compile assembly:single
build-deb:
	docker run -it --rm -v `pwd`:/app -w /app ubuntu:22.04 rm -f target/calculator_$(version)_amd64.deb \
		&& rm -rf builds/deb/usr \
		&& mkdir -p builds/deb/usr/bin \
		&& cp builds/common/calculator builds/deb/usr/bin/ \
		&& mkdir -p builds/deb/usr/bin/aa-chernyh/javafx-calculator \
		&& cp $(jar_path) builds/deb/usr/bin/aa-chernyh/javafx-calculator/ \
		&& mkdir -p builds/deb/usr/share/applications \
		&& cp builds/common/calculator.desktop builds/deb/usr/share/applications/ \
		&& mkdir -p builds/deb/usr/share/icons/hicolor/scalable/apps \
		&& cp builds/common/calculator.svg builds/deb/usr/share/icons/hicolor/scalable/apps/ \
		&& chmod -R 755 builds/deb \
		&& dpkg -b builds/deb target
build-rpm-container:
	docker build --tag 'fedora' docker/containers/fedora
build-rpm:
	docker run -it --rm -v `pwd`:/app -w /app fedora $(command)